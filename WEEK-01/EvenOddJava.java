//4 week1
import java.util.*;
class EvenOddJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a number : ");
        int num1 = input.nextInt();
        if(num1%2==0)
        {
            System.out.println("Even number ");
        }
        else
        {
            System.out.println("Odd number ");
        }

    }
}
/*
observation
    Here if i gave input.next() in the place of input.nextInt()
    then it produces error.
    Evenorodd.java:8: error: incompatible types: String cannot be converted to int*/