//13 week 4
class ConstDestJava
{
    String fullName;
    int rollnum;
    double semPercentage;
    String collegeName;
    int collegeCode;
    public ConstDestJava()
    {
         fullName = "Sai";
         rollnum = 43;
         semPercentage = 88.77;
         collegeName = "MVGR";
         collegeCode = 33;
    }
    void display()
    {
        System.out.println("name is "+fullName);
        System.out.println("rollnum is "+rollnum);
        System.out.println("semister percentage is "+semPercentage);
        System.out.println("College name is "+collegeName);
        System.out.println("college code is "+collegeCode);
    }
    public static void main(String[] args)  
    {
        ConstDestJava obj = new ConstDestJava();
        obj.display();
    }
}
/*
observation
    Here there is no need to use a destructor to destroy a object because java has automatic destructor.*/
