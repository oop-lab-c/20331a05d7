#include<iostream>
using namespace std;
class student{
    private:
    int che=1;
    protected:
    int phy=5;
    public:
    student(){
        cout<<"The marks are "<<endl;
    }
    int math=10;
        
    int getche(){
        return che;
    }
        
};
class marks1:private student{
    public:
    int getmath(){
        return math;
    }
    int getphy(){
        return phy;
    }
};
class marks2:protected student{
    public:
    int getmath(){
        return math;
    }
    int getphy(){
        return phy;
    }
};
class marks3:public student{
    public:
    int getmath(){
        return math;
    }
    int getphy(){
        return phy;
    }
};
int main()
{
    marks1 obj;
    cout<<"private inheritance"<<endl;
    cout<<obj.getmath()<<endl;//public
    cout<<obj.getphy()<<endl;//protected
    marks2 obj1;
    cout<<"protected inheritance"<<endl;
    cout<<obj1.getmath()<<endl;//public
    cout<<obj1.getphy()<<endl;//protected
    marks3 obj2;
    cout<<"public inheritance"<<endl;
    cout<<obj2.getmath()<<endl;//public
    cout<<obj2.getphy()<<endl;//protected
    return 0;
}
/*
observation
// cou<<obj.math<<endl;
    // cou<<obj.phy<<endl;
     // cou<<obj.che<<endl;
   // private methods inacessible  cout<<obj.getche()<<endl;
  // incessible cout<<obj.phy<<endl;
  //private is not accessible  cout<<obj.getche()<<endl;private
  // cant be acess directly .cout<<obj.math<<endl;
 */

