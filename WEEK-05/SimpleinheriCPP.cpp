#include<iostream>
using namespace std;
class parent{
    public:
    void fun1(){
        cout<<"I am Parent class "<<endl;
    }
};
class child : public parent{
    public:
    void fun(){
        cout<<"I am Children class"<<endl;
    }
};
int main(){
    child obj;
    obj.fun();
    obj.fun1();
    return 0;
}

/*
observation
here we created only object for child class
it access both the methods of parent and child.
*/
