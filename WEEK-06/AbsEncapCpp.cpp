//23 week 6
#include<iostream>
using namespace std;
class vary
{
    private:
    int privar;
    public:
    int pubvar;
    protected:
    int provar;
    public:
    void setvar(int a,int b,int c)
    {
        privar=a;
        pubvar=b;
        provar=c;
    }
    
    void getvar()
    {
        cout<<"The Values are "<<endl;
        cout<<privar<<endl;
        cout<<pubvar<<endl;
        cout<<provar<<endl;

    }

};
int main()
{
    vary obj;
    obj.setvar(66,77,88);
    obj.getvar();
    return 0;
}
/*
observation:
by using set and get methods also we can acess our private variables also
*/