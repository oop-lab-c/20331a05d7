//24 week 6
class AbsEncapJava
{
    public int pubvar;
    protected int provar;
    private int privar;
    void setVar(int pubvar,int provar,int privar)
    {
       this.pubvar=pubvar;
       this.provar=provar;
       this.privar=privar;
    }
    void getVar()
    {
        System.out.println("After assigning the values  :");
        System.out.println(pubvar);
        System.out.println(provar);
        System.out.println(privar);
    }
    public static void main(String[] args)
    {
        AbsEncapJava obj = new AbsEncapJava();
        obj.setVar(5,6,7);
        obj.getVar();
    }
}

/*
observation
if our passing argument and member variable is same, then we used this keyword to store actual values.
if we dont use this keyword then
    0 0 0 values print by default*/