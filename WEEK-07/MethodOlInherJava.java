//30 week7
class a
{
    void fun(int a, int b)
    {
        System.out.println(a+b);
    }
}
class MethodOlInherJava extends a 
{
    void fun(int a, int b, int c)
    {
        System.out.println(a+b+c);
    }
    public static void main(String[] args)
    {
        MethodOlInherJava obj = new MethodOlInherJava();
        obj.fun(20,20);
        obj.fun(11,22,33);
    }
    
}
