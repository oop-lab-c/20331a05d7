//32 week7
class a
{
    void show()
    {
        System.out.println("Present in parent method");
    }

}
class MethodOrJava extends a
{
    void show()
    {
        System.out.println("Present in child method");
    }
    public static void main(String[] args)
    {
        MethodOrJava obj = new MethodOrJava();
        obj.show();
    }
    
}
