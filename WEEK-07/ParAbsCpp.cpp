//29 week 7
#include<iostream>
using namespace std;
class a
{
    public:
    void display();
};
class n : public a
{
    public:
    void display()
    {
        cout<<"HAI USER ! Good morning "<<endl;
    }
};
int main()
{
    n obj;
    obj.display();
}
/*
observation
here we have to declare our function in parent class, 
and we have to define it in our child class in impure abstraction
*/