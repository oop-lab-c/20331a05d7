//28 week7
#include<iostream>
using namespace std;
class a
{
    public:

    virtual void display()=0;
};
class n : public a
{
    public:
    void display()
    {
        cout<<"have a great day "<<endl;
    }
};
int main()
{
    n obj;
    obj.display();
}
/*
observation
here pure abstraction can be provided by virtual keyword
*/