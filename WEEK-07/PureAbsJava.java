//33 week 7
interface pureabstraction
{
    void display();
}
class PureAbsJava implements pureabstraction
{
    public void display()
    {
        System.out.println("This is pureabstraction");
    }
    public static void main(String[] args)
    {
        PureAbsJava obj = new PureAbsJava();
        obj.display();
    }
}
/*
observation
if we dont use public keyword in definition of display function,error occurs
hide.java:7: error: display() in hide cannot implement display() in pureabstraction.
*/