//38 week 9
class RunnableThreadJava implements Runnable
{  
    public void run()
    {  
        for(int i=0;i<2;i++)
        {
            System.out.println("thread is formed by runnable interface");   
 
        }
    }  
    public static void main(String args[])
    {  
        RunnableThreadJava m1=new RunnableThreadJava();  
        Thread t1 =new Thread(m1); 
        t1.start();  
    }  
}  
/*
observation
if we cannot use public in run() method then error rises:
thready.java:3: error: run() in thready cannot implement run() in Runnable
    void run()
         ^
  attempting to assign weaker access privileges; was public
1 error*/