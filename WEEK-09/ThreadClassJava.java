//37 week 9
public class ThreadClassJava extends Thread
{
    public void run()
    {
        for(int i=0;i<1;i++)
        {
            int a = 81;
            int b = 9;
            System.out.println(a/b);
        }
    }
    public static void main(String[] args)
    {
        ThreadClassJava obj = new ThreadClassJava();
        obj.start();
    }
}
/*
observation
if we cannot use public in run() method then error rises:
thready.java:3: error: run() in thready cannot implement run() in Runnable
    void run()
         ^
  attempting to assign weaker access privileges; was public
1 error
*/